package de.ma.mqttdemo;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import org.junit.Test;

import de.ma.mqttdemo.EQualityOfService;

public class Demo4Test extends BaseMoquetteTestClass
{
	private static boolean PRD_CLEAN_SESSION = true;
	private static boolean RCV_CLEAN_SESSION = true;

//	@Test
//	public void if_retained_msg_send_with_QoS0_than_retained_msg_cann_be_received_afterwards()
//	throws
//		Exception
//	{
//		MqttConnectOptions connOptsProducer = new MqttConnectOptions();
//		connOptsProducer.setCleanSession( PRD_CLEAN_SESSION );
//
//		MqttClient producer1 = new MqttClient( BROKER_URL, "producerId1", new MemoryPersistence() );
//		producer1.connect( connOptsProducer );
//
//		producer1.publish( TOPIC, this.getTestPayload(), EQualityOfService.QoS0.ordinal(), RETAINED_FALSE );
//		producer1.publish( TOPIC, this.getTestPayload(), EQualityOfService.QoS0.ordinal(), RETAINED_FALSE );
//		producer1.publish( TOPIC, "LAST_GOOD_STATE".getBytes( "UTF-8" ), EQualityOfService.QoS0.ordinal(), RETAINED_TRUE );
//
//		producer1.disconnect();
//		producer1.close();
//
//		// ----------
//
//		MqttConnectOptions connOptsReceiver = new MqttConnectOptions();
//		connOptsReceiver.setCleanSession( RCV_CLEAN_SESSION );
//
//		TestCallback callback = new TestCallback();
//		MqttClient receiver1 = new MqttClient( BROKER_URL, "receiverId1", new MemoryPersistence() );
//		receiver1.connect( connOptsReceiver );
//		receiver1.setCallback( callback );
//		receiver1.subscribe( TOPIC );
//
//		// ---
//
//		assertThat( callback.getMsg(), is( notNullValue() ) );
//		assertThat( callback.getMsg().getPayload(), is( "LAST_GOOD_STATE".getBytes( "UTF-8" ) ) );
//
//		// ---
//
//		receiver1.unsubscribe( TOPIC );
//		receiver1.disconnect();
//		receiver1.close();
//	}

	@Test
	public void if_retained_msg_send_with_QoS1_than_retained_msg_can_be_received_afterwards()
	throws
		Exception
	{
		MqttConnectOptions connOptsProducer = new MqttConnectOptions();
		connOptsProducer.setCleanSession( PRD_CLEAN_SESSION );

		MqttClient producer1 = new MqttClient( BROKER_URL, "producerId1", new MemoryPersistence() );
		producer1.connect( connOptsProducer );

		producer1.publish( TOPIC, this.getTestPayload(), EQualityOfService.QoS1.ordinal(), RETAINED_FALSE );
		producer1.publish( TOPIC, this.getTestPayload(), EQualityOfService.QoS1.ordinal(), RETAINED_FALSE );
		producer1.publish( TOPIC, "LAST_GOOD_STATE".getBytes( "UTF-8" ), EQualityOfService.QoS1.ordinal(), RETAINED_TRUE );

		producer1.disconnect();
		producer1.close();

		// ----------

		MqttConnectOptions connOptsReceiver = new MqttConnectOptions();
		connOptsReceiver.setCleanSession( RCV_CLEAN_SESSION );

		TestCallback callback = new TestCallback();
		MqttClient receiver1 = new MqttClient( BROKER_URL, "receiverId1", new MemoryPersistence() );
		receiver1.connect( connOptsReceiver );
		receiver1.setCallback( callback );
		receiver1.subscribe( TOPIC );

		assertThat( callback.getMsg(), is( notNullValue() ) );
		assertThat( callback.getMsg().getPayload(), is( "LAST_GOOD_STATE".getBytes( "UTF-8" ) ) );

		receiver1.unsubscribe( TOPIC );
		receiver1.disconnect();
		receiver1.close();
	}
}
