package de.ma.mqttdemo;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.util.Date;

import org.dna.mqtt.moquette.server.Server;
import org.junit.After;
import org.junit.Before;

import de.ma.cmn.BaseTestClass;

public class BaseMoquetteTestClass extends BaseTestClass
{
	private Server server;

	@Override
	@Before
	public void before() throws Exception
	{
		this.log.debug( "b: before()" );

		super.before();

		File tmpFile = File.createTempFile( "moquette_store", ".mapdb" );
		assertThat( tmpFile.length(), is( 0L ) );

		Server.STORAGE_FILE_PATH = tmpFile.getAbsolutePath();
		this.log.debug( "dbFile: {}", Server.STORAGE_FILE_PATH );

		String configDirName = System.getProperty( "config.dir" );
		this.log.debug(  "config.dir: {}", configDirName );

		// moquette uses another property named "moquette.path" internally
		// to find password file defined in moquette config file
		// don't know why they are going this way ... :-(
		// but we use one config directory only
		System.setProperty( "moquette.path", configDirName );

		File configFile = new File( configDirName + "/moquette.conf" );

		this.server = new Server();
		this.server.startServer( configFile );

		this.log.debug( "e: before()" );
	}

	@Override
	@After
	public void after() throws Exception
	{
		this.log.debug( "b: after()" );

		if( this.server != null )
		{
			this.server.stopServer();
		}

		super.after();

		this.log.debug( "e: after()" );
	}

	protected static final String BROKER_URL = "tcp://127.0.0.1:21883";
	protected static final String TOPIC      = "Eclipse-Usergroup/Dresden";
	protected static final String PAYLOAD    = "Hallo Eclipse Usergroup Dresden";

	protected static final boolean CLEAN_SESSION_TRUE  = true;
	protected static final boolean CLEAN_SESSION_FALSE = false;

	protected static final boolean RETAINED_TRUE       = true;
	protected static final boolean RETAINED_FALSE      = false;

	protected byte[] getTestPayload()
	{
		byte[] retVal = null;
		String str    = PAYLOAD + "@" + new Date().getTime();

		try
		{
			retVal = str.getBytes( "UTF-8" );
		}
		catch( UnsupportedEncodingException e )
		{
			// nothing to do right now
			// because if UTF-8 wouldn't be supported than we can't solve this real huge problem at this place
		}

		return retVal;
	}
}
