package de.ma.mqttdemo;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import org.junit.Test;

import de.ma.mqttdemo.EQualityOfService;

public class Demo2Test extends BaseMoquetteTestClass
{
	@Test
	public void if_msg_send_and_receiver_registered_than_msg_is_received() throws Exception
	{
		this.log.debug( "b: if_msg_send_and_receiver_registered_than_msg_is_received()" );

		MqttConnectOptions connOptsProducer = new MqttConnectOptions();
		connOptsProducer.setCleanSession( CLEAN_SESSION_TRUE );

		MqttClient producer = new MqttClient( BROKER_URL, "producerId1", new MemoryPersistence() );
		producer.connect( connOptsProducer );

		// ---

		MqttConnectOptions connOptsReceiver = new MqttConnectOptions();
		connOptsReceiver.setCleanSession( CLEAN_SESSION_TRUE );

		TestCallback callback = new TestCallback();
		MqttClient receiver = new MqttClient( BROKER_URL, "receiverId1", new MemoryPersistence() );
		receiver.connect( connOptsReceiver );
		receiver.setCallback( callback );
		receiver.subscribe( TOPIC );

		// ---

		producer.publish( TOPIC, this.getTestPayload(), EQualityOfService.QoS0.ordinal(), RETAINED_FALSE );
		assertThat( callback.getMsg(), is( notNullValue() ) );

		// ---

		receiver.unsubscribe( TOPIC );
		receiver.disconnect();
		receiver.close();

		producer.disconnect();
		producer.close();

		this.log.debug( "e: if_msg_send_and_receiver_registered_than_msg_is_received()" );
	}

	@Test
	public void if_msg_send_with_QoS0_and_not_any_receiver_registered_than_msg_is_lost() throws Exception
	{
		this.log.debug( "b: if_msg_send_with_QoS0_and_not_any_receiver_registered_than_msg_is_lost()" );

		MqttConnectOptions connOptsProducer = new MqttConnectOptions();
		connOptsProducer.setCleanSession( CLEAN_SESSION_TRUE );

		MqttClient producer = new MqttClient( BROKER_URL, "producerId1", new MemoryPersistence() );
		producer.connect( connOptsProducer );

		producer.publish( TOPIC, this.getTestPayload(), EQualityOfService.QoS0.ordinal(), RETAINED_FALSE );


		// ---

		MqttConnectOptions connOptsReceiver = new MqttConnectOptions();
		connOptsReceiver.setCleanSession( CLEAN_SESSION_TRUE );

		TestCallback callback = new TestCallback();
		MqttClient receiver = new MqttClient( BROKER_URL, "receiverId1", new MemoryPersistence() );
		receiver.connect( connOptsReceiver );
		receiver.setCallback( callback );
		receiver.subscribe( TOPIC );

		// ---
		// callback.getMsg() should throw an IllegalStateException because waiting for message timed out
		// because of clean shutdown we handle this situation explicitly
		// instead using junit functionality

		boolean thrown = false;
		try
		{
			callback.getMsg();
		}
		catch( Exception e )
		{
			thrown = true;
		}

		assertThat( thrown, is( true ) );

		// ---

		receiver.unsubscribe( TOPIC );
		receiver.disconnect();
		receiver.close();

		producer.disconnect();
		producer.close();

		this.log.debug( "e: if_msg_send_with_QoS0_and_not_any_receiver_registered_than_msg_is_lost()" );
	}

	@Test
	public void if_msg_send_with_QoS1_and_not_any_receiver_registered_than_msg_is_lost() throws Exception
	{
		this.log.debug( "b: if_msg_send_with_QoS1_and_not_any_receiver_registered_than_msg_is_lost()" );

		MqttConnectOptions connOptsProducer = new MqttConnectOptions();
		connOptsProducer.setCleanSession( CLEAN_SESSION_TRUE );

		MqttClient producer = new MqttClient( BROKER_URL, "producerId1", new MemoryPersistence() );
		producer.connect( connOptsProducer );

		producer.publish( TOPIC, this.getTestPayload(), EQualityOfService.QoS1.ordinal(), RETAINED_FALSE );


		// ---

		MqttConnectOptions connOptsReceiver = new MqttConnectOptions();
		connOptsReceiver.setCleanSession( CLEAN_SESSION_TRUE );

		TestCallback callback = new TestCallback();
		MqttClient receiver = new MqttClient( BROKER_URL, "receiverId1", new MemoryPersistence() );
		receiver.connect( connOptsReceiver );
		receiver.setCallback( callback );
		receiver.subscribe( TOPIC );

		// ---
		// callback.getMsg() should throw an IllegalStateException because waiting for message timed out
		// because of clean shutdown we handle this situation explicitly
		// instead using junit functionality

		boolean thrown = false;
		try
		{
			callback.getMsg();
		}
		catch( Exception e )
		{
			thrown = true;
		}

		assertThat( thrown, is( true ) );

		// ---

		receiver.unsubscribe( TOPIC );
		receiver.disconnect();
		receiver.close();

		producer.disconnect();
		producer.close();

		this.log.debug( "e: if_msg_send_with_QoS1_and_not_any_receiver_registered_than_msg_is_lost()" );
	}

	@Test
	public void if_msg_send_with_QoS2_and_not_any_receiver_registered_than_msg_is_lost() throws Exception
	{
		this.log.debug( "b: if_msg_send_with_QoS2_and_not_any_receiver_registered_than_msg_is_lost()" );

		MqttConnectOptions connOptsProducer = new MqttConnectOptions();
		connOptsProducer.setCleanSession( CLEAN_SESSION_TRUE );

		MqttClient producer = new MqttClient( BROKER_URL, "producerId1", new MemoryPersistence() );
		producer.connect( connOptsProducer );

		producer.publish( TOPIC, this.getTestPayload(), EQualityOfService.QoS2.ordinal(), RETAINED_FALSE );


		// ---

		MqttConnectOptions connOptsReceiver = new MqttConnectOptions();
		connOptsReceiver.setCleanSession( CLEAN_SESSION_TRUE );

		TestCallback callback = new TestCallback();
		MqttClient receiver = new MqttClient( BROKER_URL, "receiverId1", new MemoryPersistence() );
		receiver.connect( connOptsReceiver );
		receiver.setCallback( callback );
		receiver.subscribe( TOPIC );

		// ---
		// callback.getMsg() should throw an IllegalStateException because waiting for message timed out
		// because of clean shutdown we handle this situation explicitly
		// instead using junit functionality

		boolean thrown = false;
		try
		{
			callback.getMsg();
		}
		catch( Exception e )
		{
			thrown = true;
		}

		assertThat( thrown, is( true ) );

		// ---

		receiver.unsubscribe( TOPIC );
		receiver.disconnect();
		receiver.close();

		producer.disconnect();
		producer.close();

		this.log.debug( "e: if_msg_send_with_QoS2_and_not_any_receiver_registered_than_msg_is_lost()" );
	}

	@Test
	public void if_msg_send_and_two_receivers_are_subscribed_than_both_receiver_get_msg() throws Exception
	{
		this.log.debug( "b: if_msg_send_and_two_receivers_are_subscribed_than_both_receiver_get_msg()" );

		MqttConnectOptions connOptsProducer = new MqttConnectOptions();
		connOptsProducer.setCleanSession( CLEAN_SESSION_TRUE );

		MqttClient producer1 = new MqttClient( BROKER_URL, "producerId1", new MemoryPersistence() );
		producer1.connect( connOptsProducer );

		// ---

		MqttConnectOptions connOptsReceiver = new MqttConnectOptions();
		connOptsReceiver.setCleanSession( CLEAN_SESSION_TRUE );

		TestCallback callback1 = new TestCallback();
		MqttClient receiver1 = new MqttClient( BROKER_URL, "receiverId1", new MemoryPersistence() );
		receiver1.connect( connOptsReceiver );
		receiver1.setCallback( callback1 );
		receiver1.subscribe( TOPIC );

		TestCallback callback2 = new TestCallback();
		MqttClient receiver2 = new MqttClient( BROKER_URL, "receiverId2", new MemoryPersistence() );
		receiver2.connect( connOptsReceiver );
		receiver2.setCallback( callback2 );
		receiver2.subscribe( TOPIC );

		// ---

		producer1.publish( TOPIC, this.getTestPayload(), EQualityOfService.QoS0.ordinal(), RETAINED_FALSE );
		assertThat( callback1.getMsg(), is( notNullValue() ) );
		assertThat( callback2.getMsg(), is( notNullValue() ) );
		assertThat( callback1.getMsg().getPayload(), is( callback2.getMsg().getPayload() ) );

		// ---

		receiver1.unsubscribe( TOPIC );
		receiver1.disconnect();
		receiver1.close();

		receiver2.unsubscribe( TOPIC );
		receiver2.disconnect();
		receiver2.close();

		producer1.disconnect();
		producer1.close();

		this.log.debug( "e: if_msg_send_and_two_receivers_are_subscribed_than_both_receiver_get_msg()" );
	}
}
