package de.ma.mqttdemo;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import org.junit.Test;

import de.ma.mqttdemo.EQualityOfService;

public class Demo5Test extends BaseMoquetteTestClass
{
	@Test
	public void if_msg_send_and_no_receiver_subscribed_and_notCleanSession_than_msg_can_be_received_afterwards()
	throws
		Exception
	{
		MqttConnectOptions connOptsProducer = new MqttConnectOptions();
		connOptsProducer.setCleanSession( CLEAN_SESSION_TRUE );

		MqttClient producer = new MqttClient( BROKER_URL, "producerId1", new MemoryPersistence() );
		producer.connect( connOptsProducer );

		// ---

		MqttConnectOptions connOptsReceiver = new MqttConnectOptions();
		connOptsReceiver.setCleanSession( CLEAN_SESSION_FALSE );

		TestCallback callback = new TestCallback();
		MqttClient receiver = new MqttClient( BROKER_URL, "receiverId1", new MemoryPersistence() );
		receiver.setCallback( callback );
		receiver.connect( connOptsReceiver );
		receiver.subscribe( TOPIC );

		// ---

		receiver.disconnect();

		producer.publish( TOPIC, this.getTestPayload(),EQualityOfService.QoS0.ordinal(), RETAINED_FALSE );

		receiver.connect( connOptsReceiver ); // subscription still active
		assertThat( callback.getMsg(), is( notNullValue() ) );

		// ---

		receiver.unsubscribe( TOPIC );
		receiver.disconnect();
		receiver.close();

		producer.disconnect();
		producer.close();
	}
}
