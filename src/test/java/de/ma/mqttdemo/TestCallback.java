package de.ma.mqttdemo;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttMessage;

public class TestCallback implements MqttCallback
{
	private CountDownLatch latch = new CountDownLatch( 1 );

	private String         topic;
	private MqttMessage    msg;

	@Override
	public void connectionLost( Throwable cause )
	{
		// nothing to do
	}

	@Override
	public void messageArrived( @SuppressWarnings("hiding") String topic, MqttMessage message ) throws Exception
	{
		this.topic = topic;
		this.msg   = message;
		this.latch.countDown();
	}

	@Override
	public void deliveryComplete( IMqttDeliveryToken token )
	{
		// nothing to do
	}

	public String getTopic() throws InterruptedException
	{
		this.latch.await();
		return this.topic;
	}

	public MqttMessage getMsg() throws InterruptedException
	{
		boolean retVal = this.latch.await( 1, TimeUnit.SECONDS );
		if( retVal != true )
		{
			throw new IllegalStateException( "timed out" );
		}
		return this.msg;
	}
}
