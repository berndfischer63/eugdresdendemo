package de.ma.cmn;

import org.junit.After;
import org.junit.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BaseTestClass
{
	static
	{
		String configDirPropName  = "config.dir";
		String configDirPropValue = "src/test/config.dev";

		System.setProperty( configDirPropName, configDirPropValue );

		org.apache.log4j.xml.DOMConfigurator.configure( configDirPropValue + "/log4j.xml" );
	}

	protected Logger log = LoggerFactory.getLogger( this.getClass() );

	@Before
	public void before() throws Exception
	{
		// present to ensure consistent class hierarchy
	}

	@After
	public void after() throws Exception
	{
		// present to ensure consistent class hierarchy
	}
}
