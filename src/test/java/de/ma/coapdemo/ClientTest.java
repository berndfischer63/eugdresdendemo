package de.ma.coapdemo;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

import org.eclipse.californium.core.CoapClient;
import org.eclipse.californium.core.CoapResponse;
import org.eclipse.californium.core.CoapServer;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import de.ma.cmn.BaseTestClass;

public class ClientTest extends BaseTestClass
{
	private static final int TEXT_PLAIN = 0;

	private CoapServer server;

	@Override
	@Before
	public void before() throws Exception
	{
		super.before();

		this.server = new CoapServer( 25683 );
		this.server.add( new HelloResource() );
		this.server.start();
	}

	@Override
	@After
	public void after() throws Exception
	{
		if( this.server != null )
		{
			this.server.stop();
			this.server.destroy();
		}

		super.after();
	}

	@Test
	public void testSimpleLocalGetRequest()
	{
		CoapClient   client = new CoapClient( "coap://localhost:25683/Hello" );
		client.useNONs();
		CoapResponse rsp = client.get();

		assertThat( rsp, is( notNullValue() ) );
		assertThat( rsp.isSuccess(), is( true ) );
		assertThat( rsp.getResponseText(), is( "Hello Eclipse Usergroup Dresden" ) );

		rsp = client.put( "Hello Eclipse Usergroup Dresden bei Tracetronic", TEXT_PLAIN );

		assertThat( rsp, is( notNullValue() ) );
		assertThat( rsp.isSuccess(), is( true ) );

		rsp = client.get();
		assertThat( rsp, is( notNullValue() ) );
		assertThat( rsp.isSuccess(), is( true ) );
		assertThat( rsp.getResponseText(), is( "Hello Eclipse Usergroup Dresden bei Tracetronic" ) );
	}
}
