package de.ma.mqttdemo;

import java.text.DecimalFormat;
import java.util.Random;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.cli.PosixParser;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

public class SensorSim
{
	private static final String REPEATCOUNT_SHORT       = "n";
	private static final String REPEATCOUNT_LONG        = "repeatCount";
	private static final String REPEATCOUNT_DESC        = "how often message should be sent";
	private static final String REPEATCOUNT_DEF         = "10";

	private static final String BROKERURL_SHORT         = "b";
	private static final String BROKERURL_LONG          = "brokerUrl";
	private static final String BROKERURL_DESC          = "mqtt broker url";
	private static final String BROKERURL_DEF           = "tcp://localhost:1883";

	private static final String DEVICENAME_SHORT        = "d";
	private static final String DEVICENAME_LONG         = "deviceName";
	private static final String DEVICENAME_DESC         = "device name";
	private static final String DEVICENAME_DEF          = "device01";

	private static final String TOPIC_SHORT             = "t";
	private static final String TOPIC_LONG              = "topic";
	private static final String TOPIC_DESC              = "topic";
	private static final String TOPIC_DEF               = "device/1/temperature";

	private static final String SLEEPMILLISECONDS_SHORT = "s";
	private static final String SLEEPMILLISECONDS_LONG  = "sleep";
	private static final String SLEEPMILLISECONDS_DESC  = "milli seconds to sleep between sending messages";
	private static final String SLEEPMILLISECONDS_DEF   = "1000";

	public static void main( String[] args ) throws Exception
	{
		Parameters params = processCmdLineParameters( args );

		MqttConnectOptions connOptsProducer = new MqttConnectOptions();
		connOptsProducer.setCleanSession( true );

		MqttClient producer = new MqttClient( params.brokerUrl, params.deviceName, new MemoryPersistence() );
		producer.connect( connOptsProducer );

		Random random   = new Random();
		double mean     = 23.0f;
		double variance = 1.0f;

		DecimalFormat df = new DecimalFormat( "#,###,###,##0.00" );

		for( int i = 0; i < params.repeatCount; ++i )
		{
			double value   = mean  + random.nextGaussian() * variance;
			String payload = df.format( value );
			producer.publish(
					params.topic,
					payload.getBytes( "UTF-8" ),
					EQualityOfService.QoS0.ordinal(),
					true
			);
			System.out.println( "sent temperatur " + payload + " to " + params.brokerUrl + "/" + params.topic );
			Thread.sleep( params.sleepMilliSeconds );
		}

		producer.disconnect();
		producer.close();
	}

	private static class Parameters
	{
		public String brokerUrl;
		public String deviceName;
		public String topic;
		public int    repeatCount;
		public int    sleepMilliSeconds;
	}

	private static Parameters processCmdLineParameters( String[] args ) throws ParseException
	{
		Options options = new Options();
		options.addOption( BROKERURL_SHORT        , BROKERURL_LONG        , true, BROKERURL_DESC         );
		options.addOption( DEVICENAME_SHORT       , DEVICENAME_LONG       , true, DEVICENAME_DESC        );
		options.addOption( TOPIC_SHORT            , TOPIC_LONG            , true, TOPIC_DESC             );
		options.addOption( REPEATCOUNT_SHORT      , REPEATCOUNT_LONG      , true, REPEATCOUNT_DESC       );
		options.addOption( SLEEPMILLISECONDS_SHORT, SLEEPMILLISECONDS_LONG, true, SLEEPMILLISECONDS_DESC );

		CommandLineParser parser  = new PosixParser();
		CommandLine       cmdLine = parser.parse( options, args );

		for( int i = 0; i < cmdLine.getOptions().length; i++ )
		{
			Option option = cmdLine.getOptions()[ i ];
			System.out.println( i + ": " + option.getOpt() + "::" + option.getValue() );
		}

		// ---

		@SuppressWarnings("synthetic-access")
		Parameters params = new Parameters();

		params.brokerUrl   = cmdLine.getOptionValue( BROKERURL_LONG, BROKERURL_DEF );
		params.deviceName  = cmdLine.getOptionValue( DEVICENAME_LONG, DEVICENAME_DEF );
		params.topic       = cmdLine.getOptionValue( TOPIC_LONG, TOPIC_DEF );

		params.repeatCount       = Integer.valueOf( cmdLine.getOptionValue( REPEATCOUNT_LONG      , REPEATCOUNT_DEF ) );
		params.sleepMilliSeconds = Integer.valueOf( cmdLine.getOptionValue( SLEEPMILLISECONDS_LONG, SLEEPMILLISECONDS_DEF ) );

		return params;
	}
}
