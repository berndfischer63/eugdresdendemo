package de.ma.mqttdemo;

public enum EQualityOfService
{
	QoS0,
	QoS1,
	QoS2
}
