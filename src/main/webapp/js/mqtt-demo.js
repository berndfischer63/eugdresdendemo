
var client = new Messaging.Client("localhost", 3000, "monitoring-dashboard");

var gauge1;
var gauge2;
$(document).ready(function () {


    gauge1 = new JustGage({
        id: "gauge1",
        value: 0,
        min: -20,
        max: 60,
        title: "Current",
        showMinMax: false,
        levelColors: ["#007CFF", "#a9d70b", "#ff0000"],
        levelColorsGradient: false,
        label: "° Celsius"
    });

    gauge2 = new JustGage({
        id: "gauge2",
        value: 0,
        min: -20,
        max: 60,
        title: "Average",
        showMinMax: false,
        levelColors: ["#007CFF", "#a9d70b", "#ff0000"],
        levelColorsGradient: false,
        label: "° Celsius"
    });


    var options = {

        //connection attempt timeout in seconds
        timeout: 3,

        //Gets Called if the connection has successfully been established
        onSuccess: function () {
            client.subscribe("device/#");
        },

        //Gets Called if the connection could not be established
        onFailure: function (message) {
            alert("Connection failed: " + message.errorMessage);
        }
    };

    //Gets called whenever you receive a message for your subscriptions
    client.onMessageArrived = function (message) {

        var topic = message.destinationName;

        if (topic === "device/1/temperature") {
            setTemperatureDevice1(message.payloadString);
        } else if (topic === "device/1/average") {
            setTemperatureDevice2(message.payloadString);
        }


    };

//Attempt to connect
    client.connect(options);
});

function setTemperatureDevice1(temp) {
    gauge1.refresh(parseFloat(temp));
}

function setTemperatureDevice2(temp) {
    gauge2.refresh(parseFloat(temp));
}